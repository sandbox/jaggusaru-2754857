Views 3D Slideshow is a module used to display your images in a CSS3 based 3D display.     
Installation 
 - Download and install as other modules.
 - Add a content type with image field.
 - Add some contents for the gallery.
 - Create a view with display format as Views Hexagon Gallery.
 -  Add title, body and image filed.
 - Save view.
