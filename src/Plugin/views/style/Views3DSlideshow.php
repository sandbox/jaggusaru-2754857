<?php

namespace Drupal\views_3d_slideshow\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_3d_slideshow",
 *   title = @Translation("Views 3D Slideshow"),
 *   help = @Translation("Displays images as 3D Slideshow."),
 *   theme = "views_3d_slideshow",
 *   display_types = {"normal"}
 * )
 */
class Views3DSlideshow extends StylePluginBase {

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Set default options
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['type'] = array('default' => 'div');
    $options['class'] = array('default' => 'container');
	
   
    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    
	$form['class'] = array(
      '#title' => $this->t('3D Gallery Wraper Class'),
      '#description' => $this->t('The class for container.Default class is bootstrap container'),
      '#type' => 'textfield',
      '#size' => '256',
      '#default_value' => $this->options['class'],
    );
  }

}
